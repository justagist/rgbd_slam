# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_pointxy.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_pointxy.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_lotsofxy.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_lotsofxy.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_offset.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_offset.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_pointxy.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_pointxy.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_pointxy_bearing.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_pointxy_bearing.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_pointxy_calib.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_pointxy_calib.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_pointxy_offset.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_pointxy_offset.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_prior.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_prior.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_twopointsxy.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_twopointsxy.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/edge_se2_xyprior.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/edge_se2_xyprior.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/parameter_se2_offset.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/parameter_se2_offset.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/types_slam2d.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/types_slam2d.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/vertex_point_xy.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/vertex_point_xy.cpp.o"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/vertex_se2.cpp" "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/types/slam2d/CMakeFiles/types_slam2d.dir/vertex_se2.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "UNIX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/core/CMakeFiles/core.dir/DependInfo.cmake"
  "/home/saif/msc_workspace/rgbd_slam/EXTERNAL/g2o/g2o/stuff/CMakeFiles/stuff.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/usr/include/eigen3"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
