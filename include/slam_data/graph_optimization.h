

#ifndef _GRAPH_OPTIM_
#define _GRAPH_OPTIM_

#include <map>
#include <slam_data/typedefs.h>
#include <slam_data/data_spot.h>

#include "g2o/core/sparse_optimizer.h"
#include "g2o/core/block_solver.h"
#include "g2o/core/factory.h"
#include "g2o/core/optimization_algorithm_factory.h"
#include "g2o/core/optimization_algorithm_gauss_newton.h"
#include "g2o/core/optimization_algorithm_levenberg.h"
#include "g2o/solvers/csparse/linear_solver_csparse.h"

#include "g2o/core/optimizable_graph.h"

//#include "g2o/solvers/cholmod/linear_solver_cholmod.h"

#include "g2o/solvers/pcg/linear_solver_pcg.h"
#include "g2o/solvers/dense/linear_solver_dense.h"
#include "g2o/core/optimization_algorithm_dogleg.h"

#include "g2o/types/slam3d/vertex_se3.h"
#include "g2o/types/slam3d/edge_se3.h"
#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/slam2d/edge_se2.h"

#include "g2o/core/robust_kernel_impl.h"

namespace rgbd_slam {

typedef g2o::BlockSolver< g2o::BlockSolverTraits<6, 3> >  SlamBlockSolver;
typedef g2o::LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearCSparseSolver;
//typedef g2o::LinearSolverCholmod<SlamBlockSolver::PoseMatrixType> SlamLinearCholmodSolver;
typedef g2o::LinearSolverPCG<SlamBlockSolver::PoseMatrixType> SlamLinearPCGSolver;
typedef g2o::LinearSolverDense<SlamBlockSolver::PoseMatrixType> SlamLinearDenseSolver;



class GraphOptimizer {
public:

    typedef data_type::MapTransformSE3 Vertices;
    typedef DataLink3D::Links Links;

    GraphOptimizer();
    ~GraphOptimizer();

    void addVertex(DataSpot3D::DataSpot3DPtr data_spot);

    void addLink(DataLink3D::DataLinkPtr link);

    void getPoses(data_type::SeqTransformSE3& out_poses);

    void updateVertices();

    void updateVertices(DataSpot3D::DataSpotMap& data_spots);

    void setVertices(const DataSpot3D::DataSpotMap& data_spots);

    void optimizeGraph(int iter = 10);

    int getVerticesSize(){
        return vertices_.size();
    }
    int getLinksSize(){
        return links_.size();
    }

    void init();
    void release();
protected:



    void addVertex(data_type::Identifier vertex_id,const data_type::TransformSE3& pose);

    void addLink(data_type::Identifier from_id, data_type::Identifier to_id,const data_type::TransformSE3& rel_transform,const data_type::InformationMatrix3D& inf_matrix = data_type::InformationMatrix3D::Identity());

public:
    Vertices vertices_;
    Links links_;

    g2o::SparseOptimizer optimizer_;
    //g2o::BlockSolverX::LinearSolverType * linear_solver_;
    //g2o::BlockSolverX * block_solver_;
    SlamBlockSolver*  block_solver_;

    g2o::RobustKernelHuber robust_kernel_;

    bool fixed_iter_;

};


}; //namespace


#endif // _GRAPH_OPTIM_
