#ifndef __TYPEDEF__
#define __TYPEDEF__

#include <iostream>
#include <Eigen/Core>
#include <vector>
#include <Eigen/StdVector> // to use the std::vector container
#include <Eigen/Dense>  // For matrix manipulations (can also visualise matrices with cout)
#include <Eigen/Geometry> // For transformations (rotation, translation, transformation matrices) -- for Eigen::Affine3d

#include <pcl/point_cloud.h>  
#include <pcl/point_types.h>  

#include <opencv2/opencv.hpp>

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>

namespace rgbd_slam
{
    namespace data_type
    {
    // shorthand types
    typedef Eigen::Affine3d TransformSE3; // 4x4 matrix of type double. Affine<Dim>d: the transformation is stored as a (Dim+1)^2 matrix, where the last row is assumed to be [0 ... 0 1].
    typedef std::vector< TransformSE3, Eigen::aligned_allocator<TransformSE3 > > SeqTransformSE3; // vector container for TransformSE3 matrices


    // Point Clouds
    typedef pcl::PointXYZ CloudPoint; // A point structure denoting Euclidean xyz coordinates
    typedef pcl::PointCloud<CloudPoint> PointCloud;
    typedef pcl::PointCloud<CloudPoint>::Ptr PointCloudPtr;

    typedef std::mutex Mutex;
    typedef std::unique_lock<Mutex> Lock;
    typedef std::condition_variable CondVar;

    typedef unsigned int Identifier;

    typedef Eigen::Matrix<double,6,6> InformationMatrix3D;
    typedef std::map<Identifier, TransformSE3, std::less<Identifier>,
                     Eigen::aligned_allocator<std::pair<const Identifier, TransformSE3> > > MapTransformSE3;
                     
    } // namespace data_type

} // namespace rgbd_slam

#endif // __TYPEDEF__