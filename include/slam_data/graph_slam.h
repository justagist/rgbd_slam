#ifndef __GRAPH_SLAM__
#define __GRAPH_SLAM__


#include "slam_data/typedefs.h"
#include "slam_data/data_spot.h"
#include "slam_data/data_bank.h"
#include "slam_data/graph_optimization.h"

namespace rgbd_slam
{
    class Graph_Slam
    {
    private:
        bool optimize_now_, optimize_near_, optimize_far_, keep_optm_thread_alive_;
        data_type::TransformSE3 map_correction_mat_;
        data_type::Mutex graph_mutex_;
        DataBank data_bank_;
        data_type::CondVar cond_var_;

        GraphOptimizer pose_graph_;
        std::thread optimize_graph_thread_;


    public:
        typedef boost::shared_ptr<Graph_Slam> Ptr;

        Graph_Slam();
        ~Graph_Slam(); 

        void processData(const data_type::TransformSE3& odom_pose,
                         const CameraParameters& cam_params,
                         const cv::Mat& rgb_img,
                         const cv::Mat& depth_img,
                         const data_type::PointCloudPtr& laser_cloud,
                         const Timestamp& tstamp);

        void notifyOptimizer(bool optimize_near, bool optimize_far); // Signals a separate thread (optmizeGraphThread) for running graph optimization

        void init();
        data_type::Mutex& getMutex(){return graph_mutex_;}
        DataBank& getDataBank() { return data_bank_; }
        void optmizeGraphThread(); 
        void saveTrajectory(const std::string& filename);

    }; //class Graph_Slam
    
}; // namespace rgbd_slam

#endif // __GRAPH_SLAM__