#ifndef __DATASPOT_H__
#define __DATASPOT_H__

#include <cstdint> // for uint32_t (but why??)
#include <sstream>
#include "slam_data/typedefs.h"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <map>

namespace rgbd_slam 
{
    template <typename TransformType, typename InfMatrixType>

    class DataLink
    {

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW

        typedef boost::shared_ptr < DataLink < TransformType, InfMatrixType >> DataLinkPtr;
        typedef std::multimap < data_type::Identifier, DataLinkPtr > Links; // multimaps can have more than onle value for each key

        enum { OdomConstraint, LoopClosureConstraint, Unspecified };

        DataLink(){};
        DataLink(data_type::Identifier from_id, data_type::Identifier to_id, const TransformType& transform, const InfMatrixType& info_mat);

        data_type::Identifier from_id_, to_id_;
        TransformType transform_;
        InfMatrixType info_mat_;

        bool active;
        int type;

    }; // class DataLink

    //Type instantiation of a data link for the 3D case
    typedef DataLink<data_type::TransformSE3, data_type::InformationMatrix3D> DataLink3D;

    class Timestamp 
    {
    public:
        uint32_t sec_,nsec_;
        Timestamp():sec_(0),nsec_(0){}
        Timestamp(uint32_t sec, uint32_t nsec):sec_(sec),nsec_(nsec){}
        // std::stringstream getTimestamp();
    };

    class CameraParameters
    {
    public:
        union 
        {
            struct{float fx_,fy_,cx_,cy_;};
            float cam_struct[4];
        };

        CameraParameters(): fx_(0),fy_(0),cx_(0),cy_(0){}
        CameraParameters(float fx, float fy, float cx, float cy):fx_(fx), fy_(fy), cx_(cx),cy_(0){}
        CameraParameters(const CameraParameters& other): fx_(other.fx_), fy_(other.fy_), cx_(other.cx_), cy_(other.cy_){}
    };

    class DataSpot3D
    {
    private:
        data_type::TransformSE3 pose_;
        CameraParameters cam_params_;
        cv::Mat rgb_img_, depth_img_;
        data_type::PointCloudPtr laser_cloud_;
        Timestamp tstamp_;

        std::vector<cv::KeyPoint> keypoints_;
        data_type::Identifier spot_id_;
        DataLink3D::Links links_;

        bool added_to_graph_;
        static data_type::Identifier static_next_id;
        
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // Required when a structure has members that are 'fixed-size vectorizable Eigen types'. It is an operator that overloads the operator new of such types

        typedef boost::shared_ptr<DataSpot3D> DataSpot3DPtr;
        typedef std::map<data_type::Identifier, DataSpot3DPtr> DataSpotMap;

        DataSpot3D(); 
        DataSpot3D(const data_type::TransformSE3& pose,
                   const CameraParameters& cam_params,
                   const cv::Mat& rgb_img,
                   const cv::Mat& depth_img,
                   const data_type::PointCloudPtr& laser_cloud);
        DataSpot3D(const data_type::TransformSE3& pose,
                   const CameraParameters& cam_params,
                   const cv::Mat& rgb_img,
                   const cv::Mat& depth_img,
                   const data_type::PointCloudPtr& laser_cloud,
                   const Timestamp& tstamp);

        std::vector<cv::KeyPoint>& getKeyPoints(){ return keypoints_ ;}

        cv::Mat& getRGBImage() { return rgb_img_; }
        cv::Mat& getDepthImage() { return depth_img_; }
        const Timestamp& getTimestamp() {return tstamp_;}

        data_type::TransformSE3 getPose() const { return pose_; }

        void addLink(const DataLink3D::DataLinkPtr& link) 
        {
        links_.insert(std::make_pair(spot_id_, link));
        }

        CameraParameters getCamParams(){ return cam_params_; }

        void setId(data_type::Identifier id) { spot_id_ = id; }

        data_type::Identifier getId() const { return spot_id_; }
        DataLink3D::Links& getLinks(){return links_;}

        void setPose(const data_type::TransformSE3& pose){pose_ = pose;}

    }; // class DataSpot3D


}; // namespace rgbd_slam

#endif // __DATASPOT_H__