#ifndef __UTILS__
#define __UTILS__

// #include <opencv2/features2d/features2d.hpp>  // might be needed when the include below is removed
#include <assert.h> // debug
#include <slam_data/typedefs.h>
#include <typeinfo> // remove
#include "slam_data/data_spot.h"
#include "slam_data/dataspot_matcher.h"

#include <pcl/registration/icp.h>
#include <pcl/registration/transformation_estimation_2D.h>
#include <pcl/sample_consensus/sac_model_registration.h>
#include <pcl/sample_consensus/ransac.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/random_sample.h>

#include <sstream>

namespace rgbd_slam
{
    namespace utils
    {
        // function to get transformation matrix from x,y,z,qx,qy,qz,qw
        data_type::TransformSE3 getTransMat (double x, double y, double z, double qx, double qy, double qz, double qw);

        template <typename scalar>
        void getTransMat (scalar x, scalar y, scalar z, scalar roll, scalar pitch, scalar yaw, Eigen::Transform<scalar, 3, Eigen::Affine> &t);

        data_type::TransformSE3 getTransMat (double x, double y, double z, double roll, double pitch, double yaw);



        data_type::CloudPoint projectDepthTo3D(const cv::Mat & depthImage,
                                            float x, float y,
                                            float cx, float cy,
                                            float fx, float fy,
                                            bool smoothing,
                                            float maxZError = 0.02f);

        float getDepth( const cv::Mat & depthImage,
                        float x, float y,
                        bool smoothing,
                        float maxZError);

        data_type::PointCloudPtr cloudFromDepth(const cv::Mat& depth_img,
                                     float cx, float cy,
                                     float fx, float fy,
                                     const std::vector<cv::KeyPoint>& kpts,
                                     const std::vector<int> indices,
                                     int decimation=1);
        data_type::PointCloudPtr cloudFromDepth(const cv::Mat & imageDepth,
                                     float cx, float cy,
                                     float fx, float fy,
                                     int decimation = 1);

        data_type::TransformSE3 estimateTransformBtwn2Spots(DataSpot3D::DataSpot3DPtr data_spot_src, DataSpot3D::DataSpot3DPtr data_spot_target,
                                                            double& variance, int& correspondences, double& prop_matches, bool& status_good,
                                                            DataSpotMatcher spot_matcher);

        Eigen::Matrix4d transformFromXYZCorrespondences(
                                                        const data_type::PointCloudPtr & cloud1,
                                                        const data_type::PointCloudPtr & cloud2,
                                                        double inlierThreshold,
                                                        int iterations,
                                                        bool refineModel,
                                                        double refineModelSigma,
                                                        int refineModelIterations,
                                                        std::vector<int> * inliersOut,
                                                        double * varianceOut);

        void getTranslationAndEulerAngles (const data_type::TransformSE3& t,
                                           float& x, float& y, float& z,
                                           float& roll, float& pitch, float& yaw);

        data_type::PointCloudPtr getICPReadyCloud(const data_type::PointCloudPtr cloud_in,
                                                  float voxel, int samples,
                                                  const data_type::TransformSE3& transform);

        data_type::PointCloudPtr voxelize(const data_type::PointCloudPtr& cloud,
                                          float voxelSize);
        data_type::PointCloudPtr sampling(const data_type::PointCloudPtr & cloud, int samples);

        data_type::PointCloudPtr getCleanCloud(data_type::PointCloudPtr cloud_in);

        void computeVariance(const data_type::PointCloudPtr & cloud_source,
                             const data_type::PointCloudPtr & cloud_target,
                             const data_type::TransformSE3& rel_transform,
                             double maxCorrespondenceDistance,
                             bool * hasConvergedOut,
                             double * variance,
                             int * correspondencesOut);

        
    }; // namespace utils
}; // namespace rgbd_slam

#endif // __UTILS__