
#ifndef __DATA_BANK__
#define __DATA_BANK__

#include "slam_data/data_spot.h"
#include "slam_data/fabmap.h"
#include "slam_data/dataspot_matcher.h"
#include "slam_data/utils.h"

namespace rgbd_slam 
{

class DataBank 
{
private:
    int near_loop_counter_, far_loop_counter_;
    FabMap fabmap_;
    int new_count_loop_far_, new_count_loop_near_; // near in time

    DataSpot3D::DataSpot3DPtr last_spot_;
    DataSpot3D::DataSpotMap data_spot_map_;

    DataSpotMatcher spot_matcher_;

public:
    typedef boost::shared_ptr<DataBank> DataBankPtr;

    DataBank();

    void addDataSpot(DataSpot3D::DataSpot3DPtr data_spot_ptr);

    int getNewLoopsCountNear() { return  new_count_loop_near_; }
    int getNewLoopsCountFar() { return  new_count_loop_far_; }
    DataSpot3D::DataSpot3DPtr getSpot(data_type::Identifier id);
    DataSpot3D::DataSpot3DPtr getLastSpot(){ return last_spot_; }
    DataSpot3D::DataSpotMap& getDataSpots() { return data_spot_map_; }

    void restartNewLoopsCountNear(){ new_count_loop_near_ = 0; }
    void restartNewLoopsCountFar(){ new_count_loop_far_ = 0; }


}; // class DataBank

}; // namespace rgbd_slam


#endif // __DATA_BANK__