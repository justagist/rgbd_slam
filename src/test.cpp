
/*  why -1 while reading image files?
    why rgb_image left in 3 channel?
    why std::bind (slam_x.cpp:line 44)?
    fabmap for loop ++l?
    transformfromXYZCorrespondences()?
    addDataSpot()? step-by-step
    computeVariance
    needoptimization in slam_x.cpp
    slam_x::init()
    g20
    links_ in data_spot.h and graph.h
*/

// #include "slam_data/data_spot.h"
#include <pcl/io/pcd_io.h> 
#include "slam_data/utils.h"
#include "slam_data/graph_slam.h"


const std::string path_prefix = "/home/saif/msc_workspace/rgbd_slam/sample_input/";
// const std::string path_prefix = " ";

int main(int argc, char const *argv[])
{
    std::cout << "here" << std::endl;       
    rgbd_slam::Graph_Slam::Ptr slam(new rgbd_slam::Graph_Slam());
    std::string odomfile_path = path_prefix + "odom_out.txt";
    FILE* odom_file = fopen("sample_input/odom_out.txt","r");
    
    cv::namedWindow("image_window");
    double tx, ty, tz, qx, qy, qz, qw, cx, cy, fx, fy;
    uint32_t sec, nsec; 

    while (fscanf(odom_file, "%u.%u %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",&sec, &nsec, &tx, &ty, &tz, &qx, &qy, &qz, &qw, &cx, &cy, &fx, &fy) == 13 )
    {    
        printf("Data Obtained. Timestamp: %u.%u\n",sec,nsec);

        // Read and store timestamp and camera parameters
        rgbd_slam::Timestamp timestamp(sec,nsec);
        rgbd_slam::CameraParameters cam_params(fx, fy, cx, cy);

        // Initialising 
        cv::Mat rgb_image, depth_image, tmp_depth_mat;
        rgbd_slam::data_type::TransformSE3 odom_pose_mat;
        rgbd_slam::data_type::PointCloudPtr pLaser_cloud(new rgbd_slam::data_type::PointCloud); //Pointer to the laser pointcloud object


        std::stringstream ss;

        ss << sec << "." << nsec;

        // Load colour image
        std::string file_prefix = ss.str();
        rgb_image = cv::imread(path_prefix+file_prefix+"_c.png",-1); // using 3 channel -- why??
        assert(rgb_image.data!=NULL);  // checking if image data is present in matrix

        // Read depth image
        tmp_depth_mat = cv::imread(path_prefix+file_prefix+"_d.png",-1);  // not using -1 gives error in utils::transformFromXYZCorrespondences [Error Message: "[pcl::SampleConsensusModelRegistration::computeSampleDistanceThreshold] Covariance matrix has NaN values! Is the input cloud finite?"]
        depth_image = cv::Mat(tmp_depth_mat.rows,tmp_depth_mat.cols,CV_32FC1,tmp_depth_mat.data); // converting depth image matrix into single channel
        assert(tmp_depth_mat.data==depth_image.data); // making sure data in the matrices are matching

        // Read PCD file to PointCloud data
        pcl::io::loadPCDFile<rgbd_slam::data_type::CloudPoint> (path_prefix + file_prefix + "_lscan.pcd", *pLaser_cloud);

        // Getting a transformation matrix for the current odometry pose
        odom_pose_mat = rgbd_slam::utils::getTransMat(tx,ty,tz,qx,qy,qz,qw);

        slam->processData(odom_pose_mat, cam_params, rgb_image, depth_image, pLaser_cloud, timestamp);

        imshow("image_window",rgb_image);
        char key = cv::waitKey(1);
        if (key == 'q') 
        {
            printf("Manually Stopped\n"); 
            break;
        }
        // break; // using for testing only. TO REMOVE.  
    } // while (reading from odom file)

    // slam->saveTrajectory("trajectory.txt");

    return 0;
} // main