
#include "slam_data/data_bank.h"

namespace rgbd_slam
{

    DataBank::DataBank():near_loop_counter_(0),far_loop_counter_(0){}

    void DataBank::addDataSpot(DataSpot3D::DataSpot3DPtr data_spot_ptr)
    {
        std::cout<<"Adding current data spot..."<< std::endl;
        
        std::cout << "Checking for Loop Closure..." << std::endl;
        int new_id, loop_id; // new id -> id of the current spot. loop_id -> id of matching spot in databank.
        fabmap_.compareAndAdd(data_spot_ptr, new_id, loop_id);
        std::cout << "LoopID: " << loop_id << std::endl;

        if (loop_id >= 0 && new_id > 0)
        {
            double variance, prop_matches;
            int correspondences;
            bool status_good = false;
            DataLink3D::DataLinkPtr link( new DataLink3D());

            DataSpot3D::DataSpot3DPtr spot_src = data_spot_map_.find(loop_id)->second;

            link->from_id_ = spot_src->getId();
            link->to_id_ = data_spot_ptr->getId();
            std::cout << "Estimating Loop Transform " << std::endl;

            link->transform_ = utils::estimateTransformBtwn2Spots(spot_src, data_spot_ptr, variance, correspondences, prop_matches, status_good, spot_matcher_);
            
            float x,y,z,roll,pitch,yaw;
            utils::getTranslationAndEulerAngles(link->transform_,x,y,z,roll,pitch,yaw);
            link->transform_ = utils::getTransMat(x,y,0,0,0,yaw); // forcing 2D

            double info = 1.0/(variance*10); // before 1.0/(variance*100);

            // info before was 100
            if( info > 0 && info < 1000000) link->info_mat_ *= info*2;
            else link->info_mat_ *= 1;
            link->active = true;
            link->type = DataLink3D::LoopClosureConstraint;
            std::cout << "LOOP: corr " <<  correspondences << " info " << info << std::endl;
            if( status_good )
            {

                bool valid = true;
                if( (new_id - loop_id) > 10 ) //far loop
                { 
                    far_loop_counter_++;
                    new_count_loop_far_++;
                }
                else 
                { 
                    near_loop_counter_++;
                    new_count_loop_near_++;
                }

                Eigen::Vector3d t0 = (spot_src->getPose()*link->transform_).translation();
                Eigen::Vector3d t1 = data_spot_ptr->getPose().translation();
                float dist = (t1-t0).norm();

                if( dist > 0.20 ) // If near loop is close in time but too far to be near, then it's not a valid transform
                { 
                    valid = false;
                }


                if( valid ) 
                {
                    spot_src->addLink(link);

                    std::cout << " LOOP ADDED ! NFar " << far_loop_counter_ << " NNear " << near_loop_counter_ << std::endl;
                }

            }

        } // loop closure check complete

        std::cout << "Adding odometry constraint..." << std::endl;

        if (last_spot_.get())
        {
            data_type::TransformSE3 rel_transform = last_spot_->getPose().inverse()*data_spot_ptr->getPose();
            
            float cx, cy, fx, fy;
            cx = last_spot_->getCamParams().cx_;
            cy = last_spot_->getCamParams().cy_;
            fx = last_spot_->getCamParams().fx_;
            fy = last_spot_->getCamParams().fy_;

            data_type::PointCloudPtr cloud_src = utils::getICPReadyCloud(utils::cloudFromDepth(last_spot_->getDepthImage(), cx, cy, fx, fy), 0.05, 0, last_spot_->getPose());

            data_type::PointCloudPtr cloud_tgt = utils::getICPReadyCloud(utils::cloudFromDepth(data_spot_ptr->getDepthImage(), cx, cy, fx, fy), 0.05, 0, data_spot_ptr->getPose());

            bool converged = false;
            double odom_variance;
            int odom_correspondences;
            utils::computeVariance(cloud_src, cloud_tgt, rel_transform, 0.05, &converged, &odom_variance, &odom_correspondences);

            double info = 1.0/(odom_variance*10);
            std::cout << "ODOM: corr " <<  odom_correspondences << " info " << info << std::endl;

            DataLink3D::DataLinkPtr link( new DataLink3D() );

            // before was 100
            if( info > 0 && info < 1000000 ) link->info_mat_ *= info;
            else if (info > 1000000) link->info_mat_ *= 1000000;//0.5;
            else link->info_mat_ *= 10;//0.5;

            link->transform_ = rel_transform;
            link->from_id_ = last_spot_->getId();
            link->to_id_ = data_spot_ptr->getId();
            link->active = true;
            link->type = DataLink3D::OdomConstraint;

            last_spot_->addLink(link);
        }

        data_spot_map_.insert(data_spot_map_.end(), std::make_pair(data_spot_ptr->getId(),data_spot_ptr));
        last_spot_ = data_spot_ptr;
        
    } // void addDataSpot

    DataSpot3D::DataSpot3DPtr DataBank::getSpot(data_type::Identifier id)
    {
        DataSpot3D::DataSpotMap::iterator it = data_spot_map_.find(id);
        if( it != data_spot_map_.end() )
            return it->second;
        else
            return DataSpot3D::DataSpot3DPtr();
    }

};//namespace rgbd_slam