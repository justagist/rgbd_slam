#include "slam_data/data_spot.h"
#include <iostream> // remove
namespace rgbd_slam
{

    // std::stringstream Timestamp::getTimestamp() // returns current timestamp as a stringstream
    // {
    //     std::stringstream ss;
    //     ss << std::to_string(this->sec_) << "." << std::to_string(this->nsec_);
    //     return ss;
    // }


    DataSpot3D::DataSpot3D() : pose_(data_type::TransformSE3::Identity()),added_to_graph_(false), spot_id_(static_next_id++)
    {
        
    }

    DataSpot3D::DataSpot3D(const data_type::TransformSE3& pose, const CameraParameters& cam_params, const cv::Mat& rgb_img,const cv::Mat& depth_img, const data_type::PointCloudPtr& laser_cloud) : pose_(pose), cam_params_(cam_params), rgb_img_(rgb_img), depth_img_(depth_img), laser_cloud_(laser_cloud), added_to_graph_(false), spot_id_(static_next_id++)
    {

    }

    DataSpot3D::DataSpot3D(const data_type::TransformSE3& pose, const CameraParameters& cam_params, const cv::Mat& rgb_img, const cv::Mat& depth_img, const data_type::PointCloudPtr& laser_cloud, const Timestamp& tstamp) : pose_(pose), cam_params_(cam_params), rgb_img_(rgb_img), depth_img_(depth_img), laser_cloud_(laser_cloud), tstamp_(tstamp), added_to_graph_(false), spot_id_(static_next_id++) 
    {

    }

    data_type::Identifier DataSpot3D::static_next_id = 0;

} //namespace rgbd_slam

