
#include "slam_data/graph_slam.h"

namespace rgbd_slam
{
    Graph_Slam::Graph_Slam(): map_correction_mat_(data_type::TransformSE3::Identity()), optimize_now_(false), keep_optm_thread_alive_(true), optimize_near_(false), optimize_far_(false){std::cout << "got";}

    Graph_Slam::~Graph_Slam()
    {
        {
            data_type::Lock lk(graph_mutex_);
            keep_optm_thread_alive_ = false;
            lk.unlock();
        }

        cond_var_.notify_all();
    }

    void Graph_Slam::init()
    { 
        optimize_now_ = false;
        keep_optm_thread_alive_ = true;
        optimize_graph_thread_ = std::thread(std::bind(&Graph_Slam::optmizeGraphThread, this)); 
    }
    void Graph_Slam::processData(const data_type::TransformSE3& odom_pose,
                                 const CameraParameters& cam_params,
                                 const cv::Mat& rgb_img,
                                 const cv::Mat& depth_img,
                                 const data_type::PointCloudPtr& laser_cloud,
                                 const Timestamp& tstamp)
    {

        graph_mutex_.lock();
        data_type::TransformSE3 corrected_pose = map_correction_mat_*odom_pose;
        graph_mutex_.unlock();

        // Creating a data_spot with the current info (as a pointer), and corrected odom_pose
        DataSpot3D::DataSpot3DPtr new_data_spot(new DataSpot3D(corrected_pose, cam_params, rgb_img, depth_img, laser_cloud, tstamp));

        bool optimization_reqd = false, optimize_near = false, optimize_far = false;
        {
            data_type::Lock lk(graph_mutex_);
            data_bank_.addDataSpot(new_data_spot);

            std::cout << std::endl <<"ADDED DATASPOT. ID: " << new_data_spot->getId() << std::endl<< "====================" << std::endl;
            
            optimize_near = data_bank_.getNewLoopsCountNear() >= 2;
            optimize_far = data_bank_.getNewLoopsCountFar() >= 10; 
            optimization_reqd = optimize_near && optimize_far;
            optimization_reqd = optimize_near;            
        }

        if (optimization_reqd)
        {
            notifyOptimizer(optimize_near,optimize_far);
        }
    }

    void Graph_Slam::notifyOptimizer(bool optimize_near, bool optimize_far)
    {
        {
            data_type::Lock lk(graph_mutex_);
            optimize_now_ = true;
            optimize_near_ = optimize_near;
            optimize_far_ = optimize_far_;
            lk.unlock();
        }
        cond_var_.notify_one();
    }

    void Graph_Slam::saveTrajectory(const std::string& filename)
    {
        //Lock graph
        data_type::Lock* lock = NULL;
        lock = new data_type::Lock(this->getMutex());

        std::ofstream output(filename);
        // Trajectory so far
        DataSpot3D::DataSpotMap& data_spots = this->getDataBank().getDataSpots();
        for(auto it = data_spots.begin(); it != data_spots.end(); it++)
        {
            data_type::TransformSE3 pose = it->second->getPose();
            const Timestamp& timestamp = it->second->getTimestamp();
            Eigen::Vector3d t = pose.translation();
            Eigen::Quaternion<double> q(pose.rotation());
            // Format: timestamp tx ty tx qx qy qz qw
            output << timestamp.sec_ << "." << timestamp.nsec_ << " " << t.x() << " " <<  t.y() << " " << t.z() << " " << q.x() << " " << q.y() << " " << q.z() << " " << q.w() <<  std::endl;
        }
        output.close();
        delete lock;
        std::cout << "Saved to file" << std::endl;
    }

    void Graph_Slam::optmizeGraphThread()
    {

        while(keep_optm_thread_alive_){


            // Wait until main() sends data
            data_type::Lock lk(graph_mutex_);
            std::cout << "Worker is waiting\n" << std::endl;
            cond_var_.wait(lk, [&]{return optimize_now_;});
            std::cout << "Worker woke up\n" << std::endl;

            // If we need to quit, we quit.
            if( !keep_optm_thread_alive_ )
                break;

            // Setting graph
            pose_graph_.setVertices(data_bank_.getDataSpots());

            // after the wait, we own the lock.
            std::cout << "Worker thread is processing optimizing graph\n";


            //
            // Important stuff being done here
            // ...
            data_type::TransformSE3 optimized_pose, old_pose;

            old_pose = data_bank_.getLastSpot()->getPose(); // Old pose before optimization

            pose_graph_.fixed_iter_ = false;
            if( optimize_near_ )
            {
                pose_graph_.optimizeGraph(10);
            }
            else if( optimize_far_ ) 
            {
                pose_graph_.fixed_iter_ = true;
                pose_graph_.optimizeGraph(50);
            }
            pose_graph_.updateVertices(data_bank_.getDataSpots());
            pose_graph_.release();

            //Just setting optimize_now_ to false, since we just optimized the graph
            optimize_now_ = false;

            optimized_pose = data_bank_.getLastSpot()->getPose(); // Now that's the new optimized pose

            map_correction_mat_ = (optimized_pose*old_pose.inverse())*map_correction_mat_;

            std::cout << "Map Correction: " << map_correction_mat_.matrix() << std:: endl;

            // Send data back to main()
            //processed = true;
            std::cout << "Worker thread signals data processing completed\n";

            // Restarting loop count
            if( optimize_near_ )
            {
                data_bank_.restartNewLoopsCountNear();
                optimize_near_ = false;
            }
            if( optimize_far_ )
            {
                data_bank_.restartNewLoopsCountFar();
                optimize_far_ = false;
            }

            // Manual unlocking is done before notifying, to avoid waking up
            // the waiting thread only to block again (see notify_one for details)
            lk.unlock();
            cond_var_.notify_one();
        }

    }
};