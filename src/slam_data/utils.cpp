#include <slam_data/utils.h>


namespace rgbd_slam
{
    namespace utils
    {

        data_type::TransformSE3 getTransMat(double x,double y,double z,double qx,double qy,double qz, double qw)
        {
            Eigen::Quaternion<double> q;
            q.x() = qx; q.y() = qy; q.z() = qz; q.w() = qw;
            Eigen::Vector3d t(x,y,z);
            Eigen::Translation<double,3> trans(t);
            // rgbd_slam::TransformSE3 trans_mat =  trans*q;
            // return trans_mat;
            return trans*q;
        }
        template < typename scalar >
        void getTransMat (scalar x, scalar y, scalar z, scalar roll, scalar pitch, scalar yaw, Eigen::Transform<scalar, 3, Eigen::Affine> &t)
        {
            scalar A = cos (yaw),  B = sin (yaw),  C  = cos (pitch), D  = sin (pitch),
                    E = cos (roll), F = sin (roll), DE = D*E,         DF = D*F;

            t (0, 0) = A*C;  t (0, 1) = A*DF - B*E;  t (0, 2) = B*F + A*DE;  t (0, 3) = x;
            t (1, 0) = B*C;  t (1, 1) = A*E + B*DF;  t (1, 2) = B*DE - A*F;  t (1, 3) = y;
            t (2, 0) = -D;   t (2, 1) = C*F;         t (2, 2) = C*E;         t (2, 3) = z;
            t (3, 0) = 0;    t (3, 1) = 0;           t (3, 2) = 0;           t (3, 3) = 1;
        }

        data_type::TransformSE3 getTransMat (double x, double y, double z, double roll, double pitch, double yaw)
        {
            data_type::TransformSE3 t;
            getTransMat (x, y, z, roll, pitch, yaw, t);
            return (t);
        }

        data_type::PointCloudPtr cloudFromDepth(const cv::Mat& depth_img,
                                     float cx, float cy,
                                     float fx, float fy,
                                     const std::vector<cv::KeyPoint>& kpts,
                                     const std::vector<int> indices,
                                     int decimation)
        {
            assert(!depth_img.empty() && (depth_img.type() == CV_16UC1 || depth_img.type() == CV_32FC1));
            assert(depth_img.rows % decimation == 0);
            assert(depth_img.cols % decimation == 0);

            data_type::PointCloudPtr cloud(new data_type::PointCloud());
            if(decimation < 1)
            {
                return cloud;
            }

            //cloud.header = cameraInfo.header;
            cloud->height = 1;
            cloud->width  = indices.size();
            cloud->is_dense = false;

            cloud->resize(cloud->height * cloud->width);

            int count = 0 ;
            for( auto it = indices.begin(); it != indices.end(); it++)
            {
                const cv::KeyPoint& keypoint = kpts[ *it ];


                data_type::CloudPoint & pt = cloud->at(count);

                data_type::CloudPoint ptXYZ = projectDepthTo3D(depth_img, keypoint.pt.x, keypoint.pt.y, cx, cy, fx, fy, false);
                pt.x = ptXYZ.x;
                pt.y = ptXYZ.y;
                pt.z = ptXYZ.z;
                ++count;

            }

            return cloud;
        }

        data_type::PointCloudPtr cloudFromDepth(const cv::Mat & imageDepth,
                                     float cx, float cy,
                                     float fx, float fy,
                                     int decimation)
        {
            assert(!imageDepth.empty() && (imageDepth.type() == CV_16UC1 || imageDepth.type() == CV_32FC1));
            assert(imageDepth.rows % decimation == 0);
            assert(imageDepth.cols % decimation == 0);

            data_type::PointCloudPtr cloud(new data_type::PointCloud());
            if(decimation < 1)
            {
                return cloud;
            }

            //cloud.header = cameraInfo.header;
            cloud->height = imageDepth.rows/decimation;
            cloud->width  = imageDepth.cols/decimation;
            cloud->is_dense = false;

            cloud->resize(cloud->height * cloud->width);

            int count = 0 ;

            for(int h = 0; h < imageDepth.rows; h+=decimation)
            {
                for(int w = 0; w < imageDepth.cols; w+=decimation)
                {
                    data_type::CloudPoint & pt = cloud->at((h/decimation)*cloud->width + (w/decimation));

                    data_type::CloudPoint ptXYZ = projectDepthTo3D(imageDepth, w, h, cx, cy, fx, fy, true);
                    pt.x = ptXYZ.x;
                    pt.y = ptXYZ.y;
                    pt.z = ptXYZ.z;
                    ++count;
                }
            }

            return cloud;
        }

        float getDepth(
                const cv::Mat & depthImage,
                float x, float y,
                bool smoothing,
                float maxZError)
        {
            assert(!depthImage.empty());
            assert(depthImage.type() == CV_16UC1 || depthImage.type() == CV_32FC1);

            int u = int(x+0.5f);
            int v = int(y+0.5f);

            if(!(u >=0 && u<depthImage.cols && v >=0 && v<depthImage.rows))
            {
                printf("ERROR: !(x >=0 && x<depthImage.cols && y >=0 && y<depthImage.rows) cond failed! returning bad point. (x=%f (u=%d), y=%f (v=%d), cols=%d, rows=%d)",
                       x,u,y,v,depthImage.cols, depthImage.rows);
                return 0;
            }

            bool isInMM = depthImage.type() == CV_16UC1; // is in mm?

            // Inspired from RGBDFrame::getGaussianMixtureDistribution() method from
            // https://github.com/ccny-ros-pkg/rgbdtools/blob/master/src/rgbd_frame.cpp
            // Window weights:
            //  | 1 | 2 | 1 |
            //  | 2 | 4 | 2 |
            //  | 1 | 2 | 1 |
            int u_start = std::max(u-1, 0);
            int v_start = std::max(v-1, 0);
            int u_end = std::min(u+1, depthImage.cols-1);
            int v_end = std::min(v+1, depthImage.rows-1);

            float depth = isInMM?(float)depthImage.at<uint16_t>(v,u)*0.001f:depthImage.at<float>(v,u);
            if(depth!=0.0f && std::isfinite(depth) && depth==depth)
            {
                if(smoothing)
                {
                    float sumWeights = 0.0f;
                    float sumDepths = 0.0f;
                    for(int uu = u_start; uu <= u_end; ++uu)
                    {
                        for(int vv = v_start; vv <= v_end; ++vv)
                        {
                            if(!(uu == u && vv == v))
                            {
                                float d = isInMM?(float)depthImage.at<uint16_t>(vv,uu)*0.001f:depthImage.at<float>(vv,uu);
                                // ignore if not valid or depth difference is too high
                                if(d != 0.0f && std::isfinite(d) && fabs(d - depth) < maxZError)
                                {
                                    if(uu == u || vv == v)
                                    {
                                        sumWeights+=2.0f;
                                        d*=2.0f;
                                    }
                                    else
                                    {
                                        sumWeights+=1.0f;
                                    }
                                    sumDepths += d;
                                }
                            }
                        }
                    }
                    // set window weight to center point
                    depth *= 4.0f;
                    sumWeights += 4.0f;

                    // mean
                    depth = (depth+sumDepths)/sumWeights;
                }
            }
            else
            {
                depth = 0;
            }
            return depth;
        }


        // Umproject stuff
        data_type::CloudPoint projectDepthTo3D( const cv::Mat & depthImage,
                                                float x, float y,
                                                float cx, float cy,
                                                float fx, float fy,
                                                bool smoothing,
                                                float maxZError)
        {
            assert(depthImage.type() == CV_16UC1 || depthImage.type() == CV_32FC1);

            data_type::CloudPoint pt;
            float bad_point = std::numeric_limits<float>::quiet_NaN ();

            float depth = getDepth(depthImage, x, y, smoothing, maxZError);
            if(depth)
            {
                // Use correct principal point from calibration
                cx = cx > 0.0f ? cx : float(depthImage.cols/2) - 0.5f; //cameraInfo.K.at(2)
                cy = cy > 0.0f ? cy : float(depthImage.rows/2) - 0.5f; //cameraInfo.K.at(5)

                // Fill in XYZ
                pt.x = (x - cx) * depth / fx;
                pt.y = (y - cy) * depth / fy;
                pt.z = depth;
            }
            else
            {
                pt.x = pt.y = pt.z = bad_point;
            }
            return pt;
        }

        data_type::TransformSE3 estimateTransformBtwn2Spots(DataSpot3D::DataSpot3DPtr data_spot_src, 
                                                            DataSpot3D::DataSpot3DPtr data_spot_target,
                                                            double& variance, int& correspondences, 
                                                            double& prop_matches, bool& status_good,
                                                            DataSpotMatcher spot_matcher)
            {
                CameraParameters src_cam = data_spot_src->getCamParams();
                CameraParameters tgt_cam = data_spot_target->getCamParams();


                DataSpotMatcher::MatchSeq matches;
                spot_matcher.match(data_spot_src, data_spot_target, matches);

                correspondences = matches.size();
                size_t tmp = std::max(data_spot_src->getKeyPoints().size(), data_spot_target->getKeyPoints().size());
                double max_points = (double)std::max(tmp,(size_t)1);
                prop_matches = double(correspondences)/max_points;
                if( correspondences < 8 ){
                    status_good = false;
                    return data_type::TransformSE3();
                }

                //Build point clouds from matches
                data_type::PointCloudPtr src_pc;
                data_type::PointCloudPtr tgt_pc;

                std::vector<int> src_indices, tgt_indices;
                src_indices.resize(matches.size());
                tgt_indices.resize(matches.size());
                int i = 0;
                for(auto it = matches.begin(); it != matches.end(); it++, i++) {
                    src_indices[i] = it->queryIdx;
                    tgt_indices[i] = it->trainIdx;
                }

                src_pc = cloudFromDepth(data_spot_src->getDepthImage(),
                                                       src_cam.cx_, src_cam.cy_, src_cam.fx_, src_cam.fy_,
                                                       data_spot_src->getKeyPoints(),
                                                       src_indices);
                tgt_pc = cloudFromDepth(data_spot_target->getDepthImage(),
                                                       tgt_cam.cx_, tgt_cam.cy_, tgt_cam.fx_, tgt_cam.fy_,
                                                       data_spot_target->getKeyPoints(),
                                                       tgt_indices);

                data_type::TransformSE3 out_transform;
                std::vector<int> inliers;
                out_transform = transformFromXYZCorrespondences(src_pc, tgt_pc, 0.05, 30, true, 3.0, 5, &inliers, &variance);

                status_good = true; // TODO: How to check if this is a good pose?

                return out_transform;
            }

        // Get transform from cloud2 to cloud1
        Eigen::Matrix4d transformFromXYZCorrespondences(const data_type::PointCloudPtr & cloud1,
                                                        const data_type::PointCloudPtr & cloud2,
                                                        double inlierThreshold,
                                                        int iterations,
                                                        bool refineModel,
                                                        double refineModelSigma,
                                                        int refineModelIterations,
                                                        std::vector<int> * inliersOut,
                                                        double * varianceOut)
            {
                //NOTE: this method is a mix of two methods:
                //  - getRemainingCorrespondences() in pcl/registration/impl/correspondence_rejection_sample_consensus.hpp
                //  - refineModel() in pcl/sample_consensus/sac.h

                if(varianceOut)
                {
                    *varianceOut = 1.0;
                }
                Eigen::Matrix4d transform;

                if(cloud1->size() >=3 && cloud1->size() == cloud2->size())
                {
                    // RANSAC
                    printf("DEBUG: iterations=%d inlierThreshold=%f\n", iterations, inlierThreshold);
                    std::vector<int> source_indices (cloud2->size());
                    std::vector<int> target_indices (cloud1->size());

                    // Copy the query-match indices
                    for (int i = 0; i < (int)cloud1->size(); ++i)
                    {
                        source_indices[i] = i;
                        target_indices[i] = i;
                    }

                    // From the set of correspondences found, attempt to remove outliers
                    // Create the registration model
                    pcl::SampleConsensusModelRegistration<data_type::CloudPoint>::Ptr model;
                    model.reset(new pcl::SampleConsensusModelRegistration<data_type::CloudPoint>(cloud2, source_indices));
                    // std::cout << source_indices.size() << "HERE" << std::endl;

                    // Pass the target_indices
                    model->setInputTarget (cloud1, target_indices);
                    // Create a RANSAC model
                    pcl::RandomSampleConsensus<data_type::CloudPoint> sac (model, inlierThreshold);
                    sac.setMaxIterations(iterations);

                    // Compute the set of inliers
                    if(sac.computeModel())
                    {
                        std::vector<int> inliers;
                        Eigen::VectorXf model_coefficients;

                        sac.getInliers(inliers);
                        sac.getModelCoefficients (model_coefficients);

                        if (refineModel)
                        {
                            double inlier_distance_threshold_sqr = inlierThreshold * inlierThreshold;
                            double error_threshold = inlierThreshold;
                            double sigma_sqr = refineModelSigma * refineModelSigma;
                            int refine_iterations = 0;
                            bool inlier_changed = false, oscillating = false;
                            std::vector<int> new_inliers, prev_inliers = inliers;
                            std::vector<size_t> inliers_sizes;
                            Eigen::VectorXf new_model_coefficients = model_coefficients;
                            do
                            {
                                // Optimize the model coefficients
                                model->optimizeModelCoefficients (prev_inliers, new_model_coefficients, new_model_coefficients);
                                inliers_sizes.push_back (prev_inliers.size ());

                                // Select the new inliers based on the optimized coefficients and new threshold
                                model->selectWithinDistance (new_model_coefficients, error_threshold, new_inliers);
                                printf("DEBUG: RANSAC refineModel: Number of inliers found (before/after): %d/%d, with an error threshold of %f.\n",
                                       (int)prev_inliers.size (), (int)new_inliers.size (), error_threshold);

                                if (new_inliers.empty ())
                                {
                                    ++refine_iterations;
                                    if (refine_iterations >= refineModelIterations)
                                    {
                                        break;
                                    }
                                    continue;
                                }

                                // Estimate the variance and the new threshold
                                double variance = model->computeVariance ();
                                error_threshold = sqrt (std::min (inlier_distance_threshold_sqr, sigma_sqr * variance));

                                printf ("DEBUG: RANSAC refineModel: New estimated error threshold: %f (variance=%f) on iteration %d out of %d.\n",
                                        error_threshold, variance, refine_iterations, refineModelIterations);
                                inlier_changed = false;
                                std::swap (prev_inliers, new_inliers);

                                // If the number of inliers changed, then we are still optimizing
                                if (new_inliers.size () != prev_inliers.size ())
                                {
                                    // Check if the number of inliers is oscillating in between two values
                                    if (inliers_sizes.size () >= 4)
                                    {
                                        if (inliers_sizes[inliers_sizes.size () - 1] == inliers_sizes[inliers_sizes.size () - 3] &&
                                                inliers_sizes[inliers_sizes.size () - 2] == inliers_sizes[inliers_sizes.size () - 4])
                                        {
                                            oscillating = true;
                                            break;
                                        }
                                    }
                                    inlier_changed = true;
                                    continue;
                                }

                                // Check the values of the inlier set
                                for (size_t i = 0; i < prev_inliers.size (); ++i)
                                {
                                    // If the value of the inliers changed, then we are still optimizing
                                    if (prev_inliers[i] != new_inliers[i])
                                    {
                                        inlier_changed = true;
                                        break;
                                    }
                                }
                            }
                            while (inlier_changed && ++refine_iterations < refineModelIterations);

                            // If the new set of inliers is empty, we didn't do a good job refining
                            if (new_inliers.empty ())
                            {
                                printf ("WARN: RANSAC refineModel: Refinement failed: got an empty set of inliers!\n");
                            }

                            if (oscillating)
                            {
                                printf("DEBUG: RANSAC refineModel: Detected oscillations in the model refinement.\n");
                            }

                            std::swap (inliers, new_inliers);
                            model_coefficients = new_model_coefficients;
                        }

                        if (inliers.size() >= 3)
                        {
                            if(inliersOut)
                            {
                                *inliersOut = inliers;
                            }
                            if(varianceOut)
                            {
                                *varianceOut = model->computeVariance();
                            }

                            // get best transformation
                            Eigen::Matrix4f bestTransformation;
                            bestTransformation.row (0) = model_coefficients.segment<4>(0);
                            bestTransformation.row (1) = model_coefficients.segment<4>(4);
                            bestTransformation.row (2) = model_coefficients.segment<4>(8);
                            bestTransformation.row (3) = model_coefficients.segment<4>(12);

                            transform = bestTransformation.cast<double>();
                            std::ostringstream ss;
                            ss << transform;
                            printf("DEBUG: RANSAC inliers=%d/%d tf=%s", (int)inliers.size(), (int)cloud1->size(),ss.str().c_str());

                            return transform.inverse(); // inverse to get actual pose transform (not correspondences transform)
                        }
                        else
                        {
                            printf("DEBUG: RANSAC: Model with inliers < 3\n");
                        }
                    }
                    else
                    {
                        printf("DEBUG: RANSAC: Failed to find model\n");
                    }
                }
                else
                {
                    printf("DEBUG: Not enough points to compute the transform\n");
                }
                return Eigen::Matrix4d();
            }

        void getTranslationAndEulerAngles (const data_type::TransformSE3& t,
                                           float& x, float& y, float& z,
                                           float& roll, float& pitch, float& yaw)
            {
                x = t(0,3);
                y = t(1,3);
                z = t(2,3);
                roll  = atan2f(t(2,1), t(2,2));
                pitch = asinf(-t(2,0));
                yaw   = atan2f(t(1,0), t(0,0));
            }

        // If "voxel" > 0, "samples" is ignored
        data_type::PointCloudPtr getICPReadyCloud(
                const data_type::PointCloudPtr cloud_in,
                float voxel,
                int samples,
                const data_type::TransformSE3 & transform)
        {

            data_type::PointCloudPtr cloud_out(new data_type::PointCloud());

            if(cloud_in->size())
            {
                if(voxel>0)
                {
                    cloud_out = voxelize(cloud_in, voxel);
                }
                else if(samples>0 && (int)cloud_in->size() > samples)
                {
                    cloud_out = sampling(cloud_in, samples);
                }

                if(cloud_out->size())
                {
                    pcl::transformPointCloud (*cloud_out, *cloud_out, transform.matrix().cast<float>());
                }
                else if( cloud_in->size() )
                {
                    pcl::transformPointCloud (*cloud_in, *cloud_out, transform.matrix().cast<float>());
                }
            }
            return cloud_out;
        }

        data_type::PointCloudPtr voxelize(const data_type::PointCloudPtr& cloud,
                                       float voxelSize)
        {
            assert(voxelSize > 0);
            data_type::PointCloudPtr output(new data_type::PointCloud());
            pcl::VoxelGrid<data_type::CloudPoint> filter;
            filter.setLeafSize(voxelSize, voxelSize, voxelSize);
            filter.setInputCloud(cloud);
            filter.filter(*output);
            return output;
        }

        data_type::PointCloudPtr sampling(const data_type::PointCloudPtr & cloud, int samples)
        {
            assert(samples > 0);
            data_type::PointCloudPtr output(new  data_type::PointCloud());
            pcl::RandomSample<data_type::CloudPoint> filter;
            filter.setSample(samples);
            filter.setInputCloud(cloud);
            filter.filter(*output);
            return output;
        }

        data_type::PointCloudPtr getCleanCloud(data_type::PointCloudPtr cloud_in)
        {
            data_type::PointCloudPtr cloud_out(new data_type::PointCloud());

            for(int i = 0; i < cloud_in->size(); i++)
            {
                data_type::CloudPoint& p = cloud_in->at(i);

                if( p.x != p.x || p.y != p.y || p.z != p.z )
                    continue;

                cloud_out->points.push_back(p);
            }
            return cloud_out;
        }

        void computeVariance(const data_type::PointCloudPtr & cloud_source,
                             const data_type::PointCloudPtr & cloud_target,
                             const data_type::TransformSE3& rel_transform, // rel_transform:how much the sensor moved w.r.t to world
                             double maxCorrespondenceDistance,
                             bool * hasConvergedOut,
                             double * variance,
                             int * correspondencesOut)
        {
            // rel_transform.inverse(): how much the world (points) moved w.r.t to the previous sensor pose
            data_type::PointCloudPtr clean_src(new data_type::PointCloud()), clean_tgt(new data_type::PointCloud());
            clean_src = getCleanCloud(cloud_source);
            clean_tgt = getCleanCloud(cloud_target);


            data_type::PointCloudPtr cloud_source_registered(new data_type::PointCloud()), clean_cloud_source_registered(new data_type::PointCloud());
            std::cout << "TRANSFORM: \n" << rel_transform.inverse().matrix() << std::endl;
            pcl::transformPointCloud(*clean_src, *cloud_source_registered, rel_transform.inverse());

            clean_cloud_source_registered = getCleanCloud(cloud_source_registered);


            std::cout << "Src Size: " << clean_cloud_source_registered->size() << " Tgt Size: " << clean_tgt->size() << std::endl;

            // compute variance
            if((correspondencesOut || variance) && clean_tgt->size() >=3 && clean_cloud_source_registered->size()>=3)
            {
                pcl::registration::CorrespondenceEstimation<data_type::CloudPoint, data_type::CloudPoint, double>::Ptr est;
                est.reset(new pcl::registration::CorrespondenceEstimation<data_type::CloudPoint, data_type::CloudPoint, double>);
                est->setInputTarget(clean_tgt);
                est->setInputSource(clean_cloud_source_registered);
                pcl::Correspondences correspondences;
                est->determineCorrespondences(correspondences, maxCorrespondenceDistance);
                if(variance)
                {
                    if(correspondences.size()>=3)
                    {
                        std::vector<double> distances(correspondences.size());
                        for(unsigned int i=0; i<correspondences.size(); ++i)
                        {
                            distances[i] = correspondences[i].distance;
                        }

                        //variance
                        std::sort(distances.begin (), distances.end ());
                        double median_error_sqr = distances[distances.size () >> 1];
                        *variance = (2.1981 * median_error_sqr);
                    }
                    else
                    {
                        *hasConvergedOut = false;
                        *variance = -1.0;
                    }
                }

                if(correspondencesOut)
                {
                    *correspondencesOut = (int)correspondences.size();
                }
            }
        }

    }; // namespace utils
}; // namespace rgbd_slam